/* Copyright 2023 coliss86
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include QMK_KEYBOARD_H
#include "keymap_french.h"
#include "sendstring_french.h"

#define DELAY_TAP_CODE 10

// Tap Dance declarations
enum {
  TAP_DANCE
};

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
  [0] = LAYOUT_1x1( TD(TAP_DANCE) )
};

void blink(void) {
  writePinHigh(C7);
  _delay_ms( 50 );
  writePinLow(C7);
  _delay_ms( 100 );
  writePinHigh(C7);
  _delay_ms( 50 );
  writePinLow(C7);
}

void dance(tap_dance_state_t *state, void *user_data) {
  if (state->count == 1) {
    writePinHigh(C7);
    SEND_STRING_DELAY(SS_TAP( X_ENTER ) "Je suis un robot O_O" SS_TAP( X_ENTER ), DELAY_TAP_CODE);
    _delay_ms( 100 );
    SEND_STRING_DELAY("Je te souhaite une bonne journ", DELAY_TAP_CODE);
    tap_code16(FR_EACU);
    _delay_ms( DELAY_TAP_CODE );
    SEND_STRING_DELAY("e" SS_TAP( X_ENTER ), DELAY_TAP_CODE);
    writePinLow(C7);
    reset_tap_dance(state);
  } else if (state->count == 2) {
    writePinHigh(C7);
    SEND_STRING_DELAY(SS_TAP( X_ENTER ) "Pour me flasher, suivre les instructions sur https://gitlab.com/coliss86/onekey"  SS_TAP( X_ENTER ), DELAY_TAP_CODE);
    writePinLow(C7);
    reset_tap_dance(state);
  } else if (state->count >= 4) {
    blink();
    reset_keyboard();
    reset_tap_dance(state);
  }
}

// Tap Dance definitions
tap_dance_action_t tap_dance_actions[] = {
  [TAP_DANCE] = ACTION_TAP_DANCE_FN(dance),
};

void keyboard_pre_init_kb (void) {
  setPinOutput(C7);
}

void keyboard_post_init_kb (void) {
  blink();
}
