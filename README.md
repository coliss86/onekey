# Onekey keeb :)

A single key keyboard with [QMK Firmware](https://github.com/qmk/qmk_firmware) based on a ATMega32u4 on a small PCB.

![onekey](doc/onekey.jpg)

Keyboard features:
- press once on the switch: a message is displayed
- press twice, another text is written
- press four times to flash it (see below).

Those messages are written in the file [keymaps/default/keymap.c](keymaps/default/keymap.c) in the function `dance()`.

## BOM 🧾

- [ ] 1 * Beetle Arduino Pro Micro
- [ ] 1 * Cherry MX switches
- [ ] 1 * 1 U Keycaps

## Electronic 📟

I glued the keycaps on the top of the electronic board and then I soldered small wired from the switch to `A1` and `D10`.

![onekey](doc/shematic.jpg)

## Firmware ⌨️

[QMK](https://github.com/qmk/qmk_firmware) is used to setup the micro-controller as a keyboard.

First, setup the [build environment setup](https://docs.qmk.fm/#/newbs_getting_started).

Then, add the source of this project to the QMK directory:
```
cd qmk_firmware/keyboards
git clone https://gitlab.com/coliss86/onekey
```

To update `qmk_firmware`, run:
```
cd qmk_firmware
git fetch -a
git checkout 0.20.0
make git-submodule
```

## Flash the firmware

- First, put the microcontroller to dfu mode by pressing the switch 4 times, the LED then blinks 2 times and glows up and down.
- Flash the keyboard by entering the following command in the `onekey` folder:

```
qmk flash -kb onekey -km default
```
